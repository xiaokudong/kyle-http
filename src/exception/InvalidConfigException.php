<?php

namespace Kyle\Http\exception;

use Exception;
use Throwable;

class InvalidConfigException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}