# kyle-http
a simple send http/https client
## 安装
``` php 
composer require kyledong/http
```
## 示例
``` php 
$client = Client::getInstance([
  //域名
  'base_uri' => '',
  //请求头
  'headers' => [],
  //false 不验证ssl，true 验证ssl
  'verify' => false
]);
//get请求
$data = $client->get('uri',[]);
//post请求
$data = $client->post('uri',[]);
//json格式请求
$data = $client->sendJson('uri',[],'GET');
```